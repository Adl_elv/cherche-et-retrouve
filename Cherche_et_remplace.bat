@echo off
setlocal enabledelayedexpansion

:debut
:: Demander le nom du fichier
echo Entrez le nom du fichier (entre guillemets si contient des espaces) :
set /p filename=

:: Supprimer les guillemets du nom du fichier
set filename=%filename:"=%

:: Vérifier si le fichier existe
if not exist "%filename%" (
    echo Le fichier specifie n'existe pas. Veuillez reessayer.
    goto debut
)

:: Demander le caractère à remplacer et le caractère de remplacement
echo Entrez le caractere a remplacer :
set /p oldChar=
echo Entrez le caractere de remplacement :
set /p newChar=

:: Extraire le nom de base et l'extension du fichier
for %%i in ("%filename%") do (
    set "name=%%~ni"
    set "extension=%%~xi"
)

:: Créer un nouveau nom de fichier pour la copie avec _modifier avant l'extension
set "newFilename=%name%_modifier%extension%"

:: Réinitialiser le fichier de sortie pour éviter d'ajouter au contenu existant
if exist "%newFilename%" del "%newFilename%"

:: Initialiser la variable pour la barre de progression
set "progress="

:: Lire le fichier ligne par ligne et remplacer le caractère
(for /f "delims=" %%a in ('type "%filename%"') do (
    set "line=%%a"
    set "modifiedLine=!line:%oldChar%=%newChar%!"
    echo !modifiedLine! >> "%newFilename%"
    
    :: Mettre à jour et afficher la barre de progression (simplifiée)
    set "progress=!progress!."
    echo Progression: !progress!
)) 

echo.
echo Operation terminee. Le fichier modifie est: %newFilename%
