# Script de Remplacement de Texte

Ce script Batch pour Windows permet de remplacer facilement un caractère par un autre dans un fichier texte, en créant une copie modifiée du fichier original. Il offre une interface simple pour spécifier le fichier à modifier, le caractère à remplacer, et par quel nouveau caractère le remplacer.

## Prérequis

- Système d'exploitation Windows.
- Les fichiers à modifier doivent être accessibles par le script.

## Utilisation
   
1. **Exécuter le Script** : Double-cliquer dessus ou clique droit -> ouvrir.

2. **Entrer les Informations Demandées** :
    - **Nom du Fichier** : Entrez le chemin complet du fichier ou naviguez jusqu'au dossier du script et entrez le nom du fichier. Utilisez des guillemets si le chemin contient des espaces.
    - **Caractère à Remplacer** : Entrez le caractère que vous souhaitez remplacer dans le fichier.
    - **Caractère de Remplacement** : Entrez le nouveau caractère qui remplacera l'ancien.

3. **Validation** : Le script affichera une barre de progression et créera une copie du fichier original avec les modifications appliquées. Le fichier modifié sera nommé en ajoutant `_modifier` avant l'extension du fichier original (par exemple, `document_modifier.txt`).

## Exemple

Entrez le nom du fichier : monFichier.txt
Entrez le caractere a remplacer : a
Entrez le caractere de remplacement : o

Après l'exécution, si `monFichier.txt` contient le texte `aaaaa Ceci est un exemple aaaaa`, le fichier `monFichier_modifier.txt` contiendra `ooooo Ceci est un exemple ooooo` avec tous les `a` remplacés par des `o`.

## Limitations

- Ce script est conçu pour fonctionner avec des fichiers texte simples. Les fichiers encodés de manière spécifique ou contenant des caractères non standard peuvent ne pas être traités correctement.
- La taille du fichier peut affecter le temps de traitement.

## Support

Pour toute question ou problème, veuillez ouvrir une issue dans le dépôt GitHub associé à ce script ou contacter le développeur.

## Licence

Ce projet est distribué sous la licence MIT.
